ruby-typhoeus (1.4.0-5+apertis0) apertis; urgency=medium

  * Sync from debian/trixie.

 -- Apertis CI <devel@lists.apertis.org>  Sun, 09 Mar 2025 23:39:22 +0000

ruby-typhoeus (1.4.0-5) unstable; urgency=medium

  * Team upload
  * Use ruby-rackup (Closes: #1094555)

 -- Sruthi Chandran <srud@debian.org>  Fri, 31 Jan 2025 14:11:33 +0100

ruby-typhoeus (1.4.0-4) unstable; urgency=medium

  * Team upload
  * Bump Standards-Version to 4.7.0 (no changes needed)
  * Build-depend on ruby-faraday-typhoeus
  * Add faraday-typhoeus.patch to use Faraday typhoeus adapter provided by the
    ruby-faraday-typhoeus package, suited for faraday 2.x
    (Closes: #1025075, #1064758)
  * Add curl-error-message.patch to fix test failure with recent curl version

 -- Cédric Boutillier <boutil@debian.org>  Sat, 31 Aug 2024 00:19:15 +0200

ruby-typhoeus (1.4.0-3+apertis0) apertis; urgency=medium

  * Sync from debian/bookworm.

 -- Apertis CI <devel@lists.apertis.org>  Sat, 08 Apr 2023 00:07:15 +0000

ruby-typhoeus (1.4.0-3) unstable; urgency=medium

  * Team upload.
  * d/control (Standards-Version): Bump to 4.6.2.
    (Depends): Use ${ruby:Depends}.
  * d/copyright: Add Upstream-Contact.
    (Copyright): Update dates.
    (Source): Use HTTPS.
  * d/ruby-tests.rake: Minor update.
  * d/watch: Change to github address.
  * d/patches/remove-rubygems-bundler.patch: Add patch.
    - Fix compatibility with Ruby 3.1.
  * d/patches/expect-tests-to-fail-with-ethon-0.16.patch: Add patch.
    - Mark some tests to fail which require adjustments due to ethon changing
      its behavior (closes: #1030406).
  * d/patches/series: Add new patch.
  * d/upstream/metadata: Add missing YAML header and Archive field.

 -- Daniel Leidert <daniel.leidert@debian.org>  Mon, 06 Feb 2023 18:01:22 +0100

ruby-typhoeus (1.4.0-2) unstable; urgency=medium

  * Team upload

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster

  [ Jenkins ]
  * Update watch file format version to 4.
  * Update standards version to 4.5.1, no changes needed.

  [ Cédric Boutillier ]
  * Update team name
  * Add .gitattributes to keep unwanted files out of the source package
  * Bump Standards-Version to 4.6.0 (no changes needed)
  * Use gem install layout
  * build-depend on ruby-webrick (Closes: #996518)

 -- Cédric Boutillier <boutil@debian.org>  Thu, 18 Nov 2021 00:28:31 +0100

ruby-typhoeus (1.4.0-1apertis0) apertis; urgency=medium

  * Import from Debian bullseye.
  * Set component to development.

 -- Apertis package maintainers <packagers@lists.apertis.org>  Thu, 22 Apr 2021 15:39:23 +0200

ruby-typhoeus (1.4.0-1) unstable; urgency=medium

  * Team Upload
  * New upstream version 1.4.0
  * Fix Control file
  * Added Upstream Metadata

 -- Abraham Raji <avronr@tuta.io>  Tue, 30 Jun 2020 10:34:39 +0530

ruby-typhoeus (1.4.0~git.20191003.0c66e4e-2) UNRELEASED; urgency=medium

  * Trim trailing whitespace.
  * Use secure copyright file specification URI.
  * Set upstream metadata fields: Bug-Database, Bug-Submit.

 -- Debian Janitor <janitor@jelmer.uk>  Mon, 20 Apr 2020 23:53:37 +0000

ruby-typhoeus (1.4.0~git.20191003.0c66e4e-1) unstable; urgency=medium

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Pirate Praveen ]
  * New upstream version 1.4.0~git.20191003.0c66e4e
  * Bump Standards-Version to 4.5.0 (no changes needed)
  * Drop compat file, rely on debhelper-compat and bump compat level to 12

 -- Pirate Praveen <praveen@debian.org>  Thu, 06 Feb 2020 12:16:27 +0100

ruby-typhoeus (1.3.1-1) unstable; urgency=medium

  * Team upload
  * New upstream release
  * Standard version updated to 4.2.1

 -- Manas Kashyap <manaskashyaptech@gmail.com>  Thu, 13 Dec 2018 06:08:41 +0000

ruby-typhoeus (1.3.0-1) unstable; urgency=medium

  * Team upload
  * New upstream release
  * Bump standards version to 4.1.4 (no changes)

 -- Manas kashyap <manaskashyaptech@gmail.com>  Sun, 13 May 2018 14:40:59 +0000

ruby-typhoeus (1.1.2-1) unstable; urgency=medium

  * Team upload
  * New upstream release

 -- Sruthi Chandran <srud@disroot.org>  Tue, 27 Jun 2017 22:51:15 +0530

ruby-typhoeus (1.1.0-1) unstable; urgency=medium

  * Team upload
  * New upstream release

 -- Sruthi Chandran <srud@disroot.org>  Fri, 09 Sep 2016 18:48:01 +0530

ruby-typhoeus (1.0.2-1) unstable; urgency=medium

  * New upstream release
  * Remove patch fix-specs-for-ruby2_3.patch (merged upstream)

 -- Pirate Praveen <praveen@debian.org>  Sat, 09 Jul 2016 20:32:36 +0530

ruby-typhoeus (0.8.0-2) unstable; urgency=medium

  * Team upload.

  [ Cédric Boutillier ]
  * Bump debhelper compatibility level to 9
  * Remove version in the gem2deb build-dependency
  * Use https:// in Vcs-* fields
  * Use https:// in Vcs-* fields
  * Bump Standards-Version to 3.9.7 (no changes needed)
  * Run wrap-and-sort on packaging files

  [ Thiago Ribeiro ]
  * Revert source code change
  * Add patch fix-spec-for-ruby2_3.patch (Closes: #816360)

 -- Thiago Ribeiro <thiago@pencillabs.com>  Tue, 15 Mar 2016 16:25:53 -0300

ruby-typhoeus (0.8.0-1) unstable; urgency=low

  * Team upload
  * New upstream release
  * Enable Tests
  * Check gemspec dependencies during build

 -- Nitesh A Jain <niteshjain92@gmail.com>  Sun, 25 Oct 2015 23:41:45 +0530

ruby-typhoeus (0.7.2-2) unstable; urgency=medium

  * Set minimum version of ruby-ethon to 0.7.4

 -- Pirate Praveen <praveen@debian.org>  Sat, 29 Aug 2015 23:49:11 +0530

ruby-typhoeus (0.7.2-1) unstable; urgency=medium

  * New upstream release

 -- Pirate Praveen <praveen@debian.org>  Sat, 29 Aug 2015 10:56:27 +0530

ruby-typhoeus (0.7.1-2) unstable; urgency=medium

  * Re-upload into unstable

 -- Pirate Praveen <praveen@debian.org>  Mon, 27 Apr 2015 15:12:29 +0530

ruby-typhoeus (0.7.1-1) experimental; urgency=medium

  * New upstream release

 -- Pirate Praveen <praveen@debian.org>  Thu, 09 Apr 2015 17:30:30 +0530

ruby-typhoeus (0.6.9-1) experimental; urgency=medium

  * New upstream release
  * Bump standards version to 3.9.6 (no changes)
  * Disable tests (some tests need rspec 3)

 -- Pirate Praveen <praveen@debian.org>  Wed, 19 Nov 2014 00:57:22 +0530

ruby-typhoeus (0.6.8-1) unstable; urgency=low

  * New upstream release.
  * Bump stadards version to 3.9.5 (no changes)
  * Bump gem2deb build dep to >= 0.7.5 (ruby 2.x)
  * Bump ruby-ethon dependency to >= 0.7.0

 -- Pirate Praveen <praveen@debian.org>  Sun, 13 Apr 2014 17:22:20 +0530

ruby-typhoeus (0.6.3-1) unstable; urgency=low

  * Initial release (Closes: #662687)

 -- Praveen Arimbrathodiyil <praveen@debian.org>  Thu, 16 May 2013 00:34:39 +0530
